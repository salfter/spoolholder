Parametric Spool Holder
=======================

I had a spool of filament that kept falling off of the spool holder I was
using, so I designed this one to be somewhat quick and economical to print,
yet effective.  Set it to produce whatever length and diameter you need; 55-
and 75-mm diameter holders that sit in the bracket atop my A8 are already
rendered to STL.
