od=70;
id=9.5;
len=80;

// cut in half...uncomment middle block for full length

module truss()
{
	polygon([[od/2-3,0],
			 [id/2,len/4],
			 [od/2-3,len/2],
//			 [id/2,3*len/4],
//			 [od/2-3,len],
//			 [od/2,len],
//			 [id/2+3,3*len/4],
			 [od/2,len/2],
			 [id/2+3,len/4],
			 [od/2,0]
	]);
}

module end_spoke()
{
	linear_extrude(3, convexity=10)
	polygon([[-1.5,id/2],
			 [1.5,id/2],
			 [1.5,od/2],
			 [-1.5,od/2]]);
}

//difference()
//{
	union() // main body
	{
		linear_extrude(len/2, convexity=10)
		{
			difference()
			{
				circle(d=od, $fn=90);
				circle(d=od-4, $fn=90);
			}

			difference()
			{
				circle(d=id+4, $fn=90);
				circle(d=id, $fn=90);
			}
		}

		// diagonal spokes
		translate([0,1.5,0])
		rotate([90,0,0])
		linear_extrude(3, convexity=10)
			truss();

		translate([-1.5,0,0])
		rotate([90,0,90])
		linear_extrude(3, convexity=10)
			truss();	
			
		translate([0,-1.5,0])
		rotate([90,0,180])
		linear_extrude(3, convexity=10)
			truss();	
			
		translate([1.5,0,0])
		rotate([90,0,-90])
		linear_extrude(3, convexity=10)
			truss();	
	}
	
//	// quick hack: cut in half so we can add flange
//	translate([0,0,len/2])
//	linear_extrude(len/2+1)
//		circle(d=od+1, $fn=90);
//}

// flange
linear_extrude(2, convexity=10)
difference()
{
	circle(d=od+20, $fn=90);
	circle(d=od, $fn=90);
}

// spokes to support the flange
end_spoke();

rotate([0,0,90])
	end_spoke();
	
rotate([0,0,180])
	end_spoke();
	
rotate([0,0,-90])
	end_spoke();