$fn=360;

spool_width=62;
spool_outer_diam_1=68;
depth_1=16;
spool_outer_diam_2=36; // supports needed if this is less than spool_outer_diam_1
inner_diam=9;

difference()
{
    union()
    {
        difference()
        {
            union()
            {
                cylinder(d=spool_outer_diam_1+20, h=2);

                translate([0,0,2])
                    cylinder(d=spool_outer_diam_1, h=depth_1);
            }

            cylinder(d=spool_outer_diam_1-4, h=2+depth_1);
        }

        for (i=[0:120:240])
        rotate([0,0,i])
        translate([0,-1,0])
            cube([spool_outer_diam_1/2-1,2,2+depth_1]);

        translate([0,0,2+depth_1])
        {
            difference()
            {
                cylinder(d=spool_outer_diam_2, h=spool_width/2-depth_1);
                cylinder(d=spool_outer_diam_2-4, h=spool_width/2-depth_1);
            }

            for (i=[0:120:240])
            rotate([0,0,i])
            translate([0,-1,0])
                cube([spool_outer_diam_2/2-1,2,spool_width/2-depth_1]);
        }
    }

    cylinder(d=inner_diam+4, h=spool_width/2+2);
}

difference()
{
    cylinder(d=inner_diam+4, h=spool_width/2+2);
    cylinder(d=inner_diam, h=spool_width/2+2);
}